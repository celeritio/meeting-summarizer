## About The Project

This application is discord bot that joins your voice channels and records your meeting in a dialog based text format.

### Built With

Used Tools and APIs :

* [Node.js](https://nodejs.org/en/docs/)
* [Google Cloud Speech to Text API](https://cloud.google.com/speech-to-text)
* [Discord API](https://discord.js.org/)

## Getting Started

Follow the process to run this application for yourself.

### Prerequisites

To Get this project up and running you need to install following tools / software on your system.

* [Node.js](https://nodejs.org)
* [Discord Server](discord.com/)
* [Google Cloud Account](https://cloud.google.com/gcp)

#### Install Discord Bot in your server :  
  
* [Install the bot in Discord Server](https://discordjs.guide/preparations/adding-your-bot-to-servers.html)

This will provide you a token key.

#### Creating credentails in google cloud :

* [Google Cloud Credentials](https://console.cloud.google.com/apis/credentials)

Copy the json file under *credentials* folder.


### Installation

1. Clone this repo

#### How to set the Application :  

1. Open terminal in DiscordClient directory, and execute following commands :
   

   ```
   npm install
   ```

#### Adding your keys to Server :

In index.js,

For, client.login('key') replace the 'key' with your new discord token key. 

For, credential_path replace the string with the relative path of your google cloud credentials.


#### How to start the Server :  
   ```
   npm run start
   ```

## Server Commands :

Go to you discord server and type the following commands as message in group channel :
(Join any audio channel before typing the commands)

1. .join
This will inform the Discord Bot to join the active voice channel. This will also enable the bot to listen your conversations and record them as text.

2. .leave
After the meeting has finished, please use this command which will inform bot to leave the voice channel.

## To Do :

1. Enahce the speech to text accuracy.
2. Setup Abstractive method to summarize the dialog based meeting.
3. Support for multiple platforms.
4. Video streaming bug fix.

