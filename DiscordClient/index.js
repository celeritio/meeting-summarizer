const fs = require('fs');
const Discord = require('discord.js');
const { Readable } = require('stream');

const SILENCE_FRAME = Buffer.from([0xF8, 0xFF, 0xFE]);

let voiceConnections = new Map();

//Google Speech to Text Variables
const credential_path = "credentials/FirstVirtualHosts-5f0ed97ed298.json"
process.env['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
// Imports the Google Cloud client library
const speech = require('@google-cloud/speech');
// Creates a client
const google_stt_client = new speech.SpeechClient();


var convert_audio = async (input) => {
    try {
        // stereo to mono channel
        const data = new Int16Array(input)
        const ndata = new Int16Array(data.length/2)
        for (let i = 0, j = 0; i < data.length; i+=4) {
            ndata[j++] = data[i]
            ndata[j++] = data[i+1]
        }
        return Buffer.from(ndata);
    } catch (e) {
        console.log(e)
        console.log('convert_audio: ' + e)
        throw e;
    }
}

class Silence extends Readable {
  _read() {
    this.push(SILENCE_FRAME);
    this.destroy();
  }
}


const client = new Discord.Client();

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async msg => {

    if (msg.content.charAt(0) === '.') {
        switch (msg.content.slice(1)) {
            case 'join':
                join(msg);
                break;
            case 'leave':
                leave(msg);
                break;
        }
    }
    
});

client.on('guildMemberSpeaking', (member, speaking) => {
    console.log(member.user.username);
    console.log("speaking - " + speaking);
    console.log("channel - " + member.voice);

    if (speaking && member.voice.channel) {
        let buffer = [];
        let voiceConnection = voiceConnections.get(member.voice.channel.id);
        let voiceReceiver = voiceConnection.receiver;
        let audio = voiceReceiver.createStream(member, { mode: 'pcm'});

        audio.on('error',  (e) => { 
            console.log('audioStream: ' + e)
        });

        audio.on('data', (data) => {
            buffer.push(data)
        })
        audio.on('end', async () => {
            buffer = Buffer.concat(buffer)
            let duration = buffer.length / 48000 / 4;
            console.log("duration: " + duration)
            //TODO : identify if audio is empty or just noise before sending it to google
            if(duration < 0)
            {
                //Skip if duraition is less the 0 sec
                return;
            }
            try {
                let new_buffer = await convert_audio(buffer)
                let out = await transcribe_gspeech(new_buffer);
                if(out && out.trim() != "")
                {
                    let sst_string = "{" + `"timestamp" : "${Date.now()}", "username" : "${member.user.username}",  "message" : "${out}"`+ "},\n";
                    fs.appendFile(member.voice.channel.id+'.txt', sst_string, function (err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });
                } 
            } catch (e) {
                console.log('tmpraw rename: ' + e)
            }    
        })

        // audio.pipe(fs.createWriteStream(member.user.username));
        
        // we have to play silence in term of reading continous audio stream working
        voiceConnection.play(new Silence(), { type: 'opus' });

    }

});

let leave = (msg) =>  {
    //TODO : leave all channels before join
    if (!msg.member || !msg.member.voice.channel) {
        msg.reply('Already Disconnected!');
        return;
    } else {
        if (voiceConnections.get(msg.member.voice.channel.id)) {
            voiceConnections.get(msg.member.voice.channel.id).disconnect();
            voiceConnections.delete(msg.member.voice.channel.id);
            // voiceReceivers.delete(msg.member.voice.channel.id);
            msg.reply('Disconnected!');
            let sst_string = "{" + `"timestamp" : "${Date.now()}", "username" : "${msg.member.user.username}",  "message" : "(DISCONNECTED)"`+ "}\n";
            let json_end =`]\n}`;
            fs.appendFile(msg.member.voice.channel.id+'.txt', sst_string + json_end, function (err) {
                if (err) throw err;
                console.log('End Coversation!');
            });

            fs.readFile(msg.member.voice.channel.id+'.txt', 'utf8' , (err, data) => {
                if (err) {
                  console.error(err)
                  return
                }
                let conversation_string = FormatConversation(data);
                msg.reply(conversation_string);
                // Send email
            });
        }
    }
}

let FormatConversation = (data) => {
    let data_string = JSON.parse(data);
    let result = "";
    for(let item in data_string.conversation)
    {
        let time = new Date(new Number(data_string.conversation[item].timestamp)).toTimeString();
        result += `[${time}] ${data_string.conversation[item].username} : ${data_string.conversation[item].message}\n`;  
    }
    return result;
}

let join = (msg) => {
    if (!msg.member || !msg.member.voice.channel) {
        msg.reply('First join the voice channel');
        return;
    } else {
        msg.member.voice.channel.join().then((voiceConnection) => {
            msg.reply('Bot has joined the audio channel [' + msg.member.voice.channel.name + ']');
            voiceConnections.set(msg.member.voice.channel.id, voiceConnection);
            let json_init = `{ "conversation" :\n[` ;
            fs.writeFile(msg.member.voice.channel.id+'.txt', json_init, function (err) {
                if (err) throw err;
                console.log('Created new file!');
            }); 
        }).catch(console.error);
    }
}

var transcribe_gspeech = async (buffer) => {
    try {
        console.log('transcribe_gspeech')
        const bytes = buffer.toString('base64');
        const audio = {
          content: bytes,
        };
        const config = {
          encoding: 'LINEAR16',
          sampleRateHertz: 48000,
          languageCode: 'en-US',
        };
        const request = {
          audio: audio,
          config: config,
        };
  
        const [response] = await google_stt_client.recognize(request);
        const transcription = response.results
          .map(result => result.alternatives[0].transcript)
          .join('\n');
        console.log(`gspeech: ${transcription}`);
        return transcription;
  
    } catch (e) { console.log('transcribe_gspeech 368:' + e) }
  }


// client.login('ODEyNjE2MTkzOTIzMDg4Mzk0.YDDVzg.Ih1aZ34XZ6Jr5k9BjXZ514S_A94');
client.login('ODIwMTc4MzUxNTA2NzE4NzQw.YExYnQ.N9LZEkDUBF-jG3YezAae8y_5LBw')